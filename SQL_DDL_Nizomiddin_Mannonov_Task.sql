CREATE DATABASE EcommerceDB;

\c EcommerceD

CREATE SCHEMA EcommerceSchema;

CREATE TABLE EcommerceSchema.Products (
    ProductID SERIAL PRIMARY KEY,
    ProductName VARCHAR(255),
    Price DECIMAL(10, 2),
    CategoryID INT
);

CREATE TABLE EcommerceSchema.Categories (
    CategoryID SERIAL PRIMARY KEY,
    CategoryName VARCHAR(100),
    Description TEXT
);

ALTER TABLE EcommerceSchema.Products
ADD CONSTRAINT FK_Products_Category
FOREIGN KEY (CategoryID) REFERENCES EcommerceSchema.Categories(CategoryID);

INSERT INTO EcommerceSchema.Categories (CategoryName, Description) VALUES
    ('Electronics', 'Electronic gadgets and devices'),
    ('Clothing', 'Fashion apparel and accessories'),
    ('Home & Kitchen', 'Household items and appliances');

INSERT INTO EcommerceSchema.Products (ProductName, Price, CategoryID) VALUES
    ('Smartphone', 599.99, 1),
    ('Laptop', 999.99, 1),
    ('T-Shirt', 19.99, 2),
    ('Coffee Maker', 49.99, 3);

SELECT * FROM EcommerceSchema.Products;
SELECT * FROM EcommerceSchema.Categories;

/// task 2

CREATE TABLE Books (
    BookID SERIAL PRIMARY KEY,
    Title VARCHAR(255) NOT NULL,
    ISBN VARCHAR(13) UNIQUE,
    PublicationYear SMALLINT,
    PublisherID INT
);

CREATE TABLE Authors (
    AuthorID SERIAL PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL
);

CREATE TABLE Publishers (
    PublisherID SERIAL PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Location VARCHAR(100)
);

//// task 3

CREATE TABLE Authors (
    AuthorID SERIAL PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL
);

CREATE TABLE Publishers (
    PublisherID SERIAL PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Location VARCHAR(100)
);

CREATE TABLE Books (
    BookID SERIAL PRIMARY KEY,
    Title VARCHAR(255) NOT NULL,
    ISBN CHAR(13) UNIQUE NOT NULL,
    PublicationYear SMALLINT,
    PublisherID INT REFERENCES Publishers(PublisherID) NOT NULL,
    AddedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    LastUpdated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT CHK_PublicationYear CHECK (PublicationYear >= 0),
    CONSTRAINT CHK_YearPublished CHECK (PublicationYear <= EXTRACT(YEAR FROM CURRENT_DATE))
);

//// task 4


CREATE TABLE Publishers (
    PublisherID SERIAL PRIMARY KEY,
    Name VARCHAR(100) NOT NULL,
    Location VARCHAR(100)
);

CREATE TABLE Books (
    BookID SERIAL PRIMARY KEY,
    Title VARCHAR(255) NOT NULL,
    ISBN CHAR(13) UNIQUE NOT NULL,
    PublicationYear SMALLINT,
    PublisherID INT REFERENCES Publishers(PublisherID) NOT NULL,
    AuthorID INT REFERENCES Authors(AuthorID) NOT NULL,
    AddedDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    LastUpdated TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT CHK_PublicationYear CHECK (PublicationYear >= 0),
    CONSTRAINT CHK_YearPublished CHECK (PublicationYear <= EXTRACT(YEAR FROM CURRENT_DATE))
);


CREATE TABLE Authors (
    AuthorID SERIAL PRIMARY KEY,
    FirstName VARCHAR(50) NOT NULL,
    LastName VARCHAR(50) NOT NULL
);


CREATE TABLE BookAuthors (
    BookID INT REFERENCES Books(BookID) NOT NULL,
    AuthorID INT REFERENCES Authors(AuthorID) NOT NULL,
    PRIMARY KEY (BookID, AuthorID)
);


///// task 5

ALTER TABLE Books
ADD CONSTRAINT CHK_PublicationDate
CHECK (to_timestamp(PublicationYear::text, 'YYYY') > '2000-01-01'::timestamp);


ALTER TABLE Books
ADD CONSTRAINT CHK_PriceNotNegative
CHECK (PublicationYear >= 0);

ALTER TABLE Authors
ADD CONSTRAINT CHK_Gender
CHECK (Gender IN ('Male', 'Female'));

ALTER TABLE Books
ADD CONSTRAINT UQ_ISBN
UNIQUE (ISBN);

ALTER TABLE Books
ADD CONSTRAINT CHK_TitleNotNull
CHECK (Title IS NOT NULL);


//// task 6

INSERT INTO Publishers (Name, Location)
VALUES
    ('Publisher A', 'New York, NY'),
    ('Publisher B', 'Los Angeles, CA'),
    ('Publisher C', 'Chicago, IL');

INSERT INTO Authors (FirstName, LastName)
VALUES
    ('John', 'Doe'),
    ('Jane', 'Smith'),
    ('Michael', 'Johnson'),
    ('Emily', 'Brown');

INSERT INTO Books (Title, ISBN, PublicationYear, PublisherID, AuthorID)
VALUES
    ('Book 1', '9781234567890', 2005, 1, 1),
    ('Book 2', '9780987654321', 2010, 2, 2),
    ('Book 3', '9781112223333', 2002, 3, 3),
    ('Book 4', '9784445556666', 2007, 1, 4),
    ('Book 5', '9787778889999', 2015, 2, 2),
    ('Book 6', '9780001112222', 2012, 3, 1),
    ('Book 7', '9783334445555', 2017, 1, 2),
    ('Book 8', '9786667778888', 2009, 2, 3),
    ('Book 9', '9789990001111', 2014, 3, 4),
    ('Book 10', '9781230005555', 2003, 1, 1);


///// task 7

ALTER TABLE Books
ADD COLUMN record_ts DATE DEFAULT current_date;

SELECT * FROM Books;

ALTER TABLE Authors
ADD COLUMN record_ts DATE DEFAULT current_date;

SELECT * FROM Authors;

ALTER TABLE Publishers
ADD COLUMN record_ts DATE DEFAULT current_date;

SELECT * FROM Publishers;


